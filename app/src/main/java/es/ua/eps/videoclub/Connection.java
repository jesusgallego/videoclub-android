package es.ua.eps.videoclub;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by mastermoviles on 27/1/17.
 */
public class Connection {
    public static String peticionGET( String strUrl )
    {
        HttpURLConnection http = null;
        String content = null;
        try {
            URL url = new URL( strUrl );
            http = (HttpURLConnection)url.openConnection();
            http.setRequestProperty("Content-Type", "application/json");
            http.setRequestProperty("Accept", "application/json");

            if( http.getResponseCode() == HttpURLConnection.HTTP_OK ) {
                StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader( http.getInputStream() ));
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                content = sb.toString();
                reader.close();
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            if( http != null ) http.disconnect();
        }
        return content;
    }
    public static Bitmap descargarImagen(String strUrl) {
        HttpURLConnection http = null;
        Bitmap bitmap = null;

        try {
            URL url = new URL( strUrl );
            http = (HttpURLConnection)url.openConnection();

            if( http.getResponseCode() == HttpURLConnection.HTTP_OK )
                bitmap = BitmapFactory.decodeStream(http.getInputStream());
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            if( http != null )
                http.disconnect();
        }
        return bitmap;
    }
}
