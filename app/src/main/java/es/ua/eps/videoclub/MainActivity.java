package es.ua.eps.videoclub;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView filmListView;

    private final String getListURL = "http://gbrain.dlsi.ua.es/videoclub/api/v1/catalog";
    List<Movie> list;
    MovieAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        filmListView = (ListView) findViewById(R.id.filmListView);

        list = new ArrayList<>();
        new DownloadTask().execute(getListURL);

        adapter = new MovieAdapter(this, list);
        filmListView.setAdapter(adapter);


        filmListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                Movie m = list.get(position);
                m.setBitmap(null);
                intent.putExtra(DetailActivity.EXTRA_MOVIE, m);
                startActivity(intent);
            }
        });
    }

    private class DownloadTask extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls)
        {
            // Llamada al método de descarga de contenido que
            // se ejecutará en segundo plano
            return Connection.peticionGET( urls[0] );
        }

        @Override
        protected void onPreExecute()
        {
            // Inicializar campos y valores necesarios
        }

        @Override
        protected void onPostExecute(String contenido)
        {
            // Mostrar el resultado en un TextView
            try {
                JSONArray array = new JSONArray(contenido);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject movieJSON = array.getJSONObject(i);
                    Movie movie = new Movie(movieJSON);
                    list.add(movie);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            adapter.notifyDataSetChanged();
        }

        @Override
        protected void onCancelled()
        {
            // Tarea cancelada, lo dejamos como estaba
        }
    }
}
