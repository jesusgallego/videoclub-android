package es.ua.eps.videoclub;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    public static final String EXTRA_MOVIE = "EXTRA_MOVIE";

    Movie movie;

    ImageView poster;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        movie = (Movie) getIntent().getSerializableExtra(EXTRA_MOVIE);

        poster = (ImageView) findViewById(R.id.detail_image);
        TextView title = (TextView) findViewById(R.id.detail_title);
        TextView director = (TextView) findViewById(R.id.detail_director);
        TextView year = (TextView) findViewById(R.id.detail_year);
        TextView synopsis = (TextView) findViewById(R.id.detail_synopsis);
        TextView rented = (TextView) findViewById(R.id.detail_rent);
        Button rentButton = (Button) findViewById(R.id.detail_rent_button);

        title.setText(movie.getTitle());
        director.setText(movie.getDirector());
        year.setText(movie.getYear());
        synopsis.setText(movie.getSynopsis());
        rented.setText(movie.isRented() ? "Película alquilada" : "Película disponible");
        rentButton.setText(movie.isRented() ? "Devolver" : "Alquilar");

        new CargarImagenTask().execute();

        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private class CargarImagenTask extends AsyncTask<Object, Integer, Bitmap>
    {
        @Override
        protected Bitmap doInBackground(Object... params) {
            return Connection.descargarImagen(movie.getPoster());
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if(result!=null) {
                movie.setBitmap(result);
                poster.setImageBitmap(result);
            }
        }
    }
}


