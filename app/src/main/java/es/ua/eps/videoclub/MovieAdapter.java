package es.ua.eps.videoclub;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mastermoviles on 27/1/17.
 */
public class MovieAdapter extends BaseAdapter{
    List<Movie> mList;
    Context mContext;

    Map<Movie, CargarImagenTask> mImagenesCargando;

    public MovieAdapter(Context context, List<Movie> list) {
        this.mContext = context;
        this.mList = list;
        this.mImagenesCargando = new HashMap<Movie, CargarImagenTask>();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.list_item, null);
        }

        TextView tvTexto = (TextView) convertView.findViewById(R.id.textView);
        ImageView ivIcono = (ImageView) convertView.findViewById(R.id.imageView);

        Movie movie = mList.get(position);
        tvTexto.setText(movie.getTitle());

        if (movie.getBitmap() != null) {
            ivIcono.setImageBitmap(movie.getBitmap());
        } else {
            if(mImagenesCargando.get(movie)==null) {

                ivIcono.setImageResource(R.mipmap.ic_launcher);

                CargarImagenTask task = new CargarImagenTask();
                mImagenesCargando.put(movie, task);
                task.execute(movie, ivIcono);
            }
        }

        return convertView;
    }


}

class CargarImagenTask extends AsyncTask<Object, Integer, Bitmap>
{
    Movie movie;
    ImageView view;

    @Override
    protected Bitmap doInBackground(Object... params) {
        this.movie = (Movie) params[0];
        this.view = (ImageView) params[1];
        return Connection.descargarImagen(movie.getPoster());
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        if(result!=null) {
            this.movie.setBitmap(result);
            this.view.setImageBitmap(result);
        }
    }
}